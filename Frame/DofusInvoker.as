﻿package {
    import flash.display.Sprite;
    import flash.utils.*;
    import flash.net.*;
    import __AS3__.vec.*;
    import flash.events.*;
    import flash.errors.IOError;
    import flash.utils.getDefinitionByName;
    import flash.system.ApplicationDomain;
    import com.Definition;
    import com.frames.*;
    import com.frames.plugbot.*;
    import com.Utils;

    public class DofusInvoker extends Sprite {
        protected static const _log:* = Definition.Log.getLogger(getQualifiedClassName(DofusInvoker));


        public function DofusInvoker() {
            try {
                _log.info("PlugBot initialized!");
                // Disable sentry
                Definition.Log["_sentryTarget"] = null;

                if (!Definition.Kernel.getWorker().contains(CatchAllAuthorizedCommandFrame))
                    Definition.Kernel.getWorker().addFrame(new CatchAllAuthorizedCommandFrame());

                Definition.Kernel.getWorker().addFrame(new PlugBotFrame());
            } catch (e:*) {
                _log.error(e.toString());
            }
        }
    }

}
