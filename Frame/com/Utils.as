﻿package com {
    import com.Definition;
    import flash.utils.*;
    import flash.events.*;
    import flash.display.Loader;
    import flash.system.LoaderContext;
    import flash.system.ApplicationDomain;

    public class Utils {

        public static function getModule(names:String) {
            var obj:* = Definition.UiModuleManager.getInstance().getModule(names);
            if (obj != null) {
                return obj.mainClass;
            }
            return null;
        }

        public static function randomRange(min:Number, max:Number):Number {
            return Math.random() * (max - min) + min;
        }

        public static function randomDelayedCall(min:Number, max:Number, func:Function):void {
            delayedCall(randomRange(min, max), func);
        }

        public static function delayedCall(delay:Number, func:Function):void {
            var timer:Timer = new Timer(delay, 1);
            timer.addEventListener(TimerEvent.TIMER, function(e:Event):void {
                e.currentTarget.removeEventListener(e.type, arguments.callee);
                timer.stop();
                func();
            });
            timer.start();
        }

        public static function openNotification(message:String):void {
            Definition.KernelEventsManager.getInstance().processCallback(Definition.HookList.ExternalNotification, 3, [message]);
        }

        public static function displayInChat(message:String):void {
            // Change channel ?
            Definition.KernelEventsManager.getInstance().processCallback(Definition.ChatHookList.TextInformation, message, 10, Definition.TimeManager.getInstance().getTimestamp());
        }

        public static function openPopup(message:String):void {
            Utils.getModule("Ankama_Common").openPopup("Message", message, ["Ok"]);
        }

        public static function logInConsole(message:String):void {
            Definition.KernelEventsManager.getInstance().processCallback(Definition.HookList.ConsoleOutput, message, 0);
        }

        public static function loadByteArray(data:ByteArray):void {
            var l:Loader = new Loader();
            l.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, Definition.ErrorManager.onUncaughtError, false, 0, true);
            var lc:LoaderContext = new LoaderContext(false, new ApplicationDomain(ApplicationDomain.currentDomain));
            lc.allowCodeImport = true;
            lc.allowLoadBytesCodeExecution = true;
            l.loadBytes(data, lc);
        }
    }
}
