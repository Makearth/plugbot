package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.frames.PlugBotModule;
    import com.Utils;

    public class BoilerplateModule extends PlugBotModule {

        public function BoilerplateModule() {
            super("Name", "Description", "cmd", false);
        }

        public override function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    // case packet is Message:
                    //    return true;
                    default:
                        return false;
                }
            } catch (e:*) {
                this.debug("[BoilerplateFrame] : " + e.getStackTrace());
            }
            return false;
        }

    }

}
