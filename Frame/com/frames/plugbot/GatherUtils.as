﻿package com.frames.plugbot {
    import flash.utils.Dictionary;
    import com.Definition;

    public class GatherUtils {
        private static var DirectionsEnum:* = Definition.DirectionsEnum;
        public static var MapDirection:Dictionary = new Dictionary();
        public static var RoundCounter:Dictionary = new Dictionary();

        public static var AlchimisteRessourceIds:Vector.<*> = new <*>[{id: 254, level: 1, name: "Ortie"},
            {id: 255, level: 20, name: "Sauge"},
            {id: 67, level: 40, name: "Trefle a 5 feuilles"},
            {id: 66, level: 60, name: "Menthe Sauvage"},
            {id: 68, level: 80, name: "Orchidée Freyesque"},
            {id: 61, level: 100, name: "Edelweiss"},
            {id: 112, level: 120, name: "Graine de Pandouille"},
            {id: 256, level: 140, name: "Ginseng"},
            {id: 257, level: 160, name: "Belladone"},
            {id: 258, level: 180, name: "Mandragore"},
            {id: 131, level: 200, name: "Perce-neige"}];
        public static var BucheronRessourceIds:Vector.<*> = new <*>[{id: 1, level: 1, name: "Frene"},
            {id: 33, level: 20, name: "Chataignier"},
            {id: 34, level: 40, name: "Noyer"},
            {id: 8, level: 60, name: "Chene"},
            {id: 98, level: 70, name: "Bombu"},
            {id: 31, level: 80, name: "Erable"},
            {id: 101, level: 90, name: "Oliviolet"},
            {id: 28, level: 100, name: "If"},
            {id: 108, level: 110, name: "Bambou"},
            {id: 35, level: 120, name: "Merisier"},
            {id: 259, level: 130, name: "Noisetier"},
            {id: 29, level: 140, name: "Ebene"},
            {id: 121, level: 150, name: "Kaliptus"},
            {id: 32, level: 160, name: "Charme"},
            {id: 109, level: 170, name: "Bambou Sombre"},
            {id: 30, level: 180, name: "Orme"},
            {id: 110, level: 190, name: "Bambou Sacré"},
            {id: 133, level: 200, name: "Tremble"}];

        public static var MineurRessourceIds:Vector.<*> = new <*>[{id: 17, level: 1, name: "Fer"},
            {id: 53, level: 20, name: "Cuivre"},
            {id: 55, level: 40, name: "Bronze"},
            {id: 37, level: 60, name: "Kobalte"},
            {id: 54, level: 80, name: "Manganese"},
            {id: 52, level: 100, name: "Etain"},
            {id: 114, level: 100, name: "Silicate"},
            {id: 24, level: 120, name: "Argent"},
            {id: 26, level: 140, name: "Bauxite"},
            {id: 25, level: 160, name: "Or"},
            {id: 113, level: 180, name: "Dolomite"},
            {id: 135, level: 200, name: "Obsidienne"}];

        public static var PaysansRessourceIds:Vector.<*> = new <*>[{id: 38, level: 1, name: "Blé"},
            {id: 43, level: 20, name: "Orge"},
            {id: 45, level: 40, name: "Avoine"},
            {id: 39, level: 60, name: "Houblon"},
            {id: 42, level: 80, name: "Lin"},
            {id: 44, level: 100, name: "Seigle"},
            {id: 111, level: 100, name: "Riz"},
            {id: 47, level: 120, name: "Malt"},
            {id: 46, level: 140, name: "Chanvre"},
            {id: 260, level: 160, name: "Mais"},
            {id: 261, level: 180, name: "Millet"},
            {id: 134, level: 200, name: "Frostiz"}];
        public static var PecheurRessourceIds:Vector.<*> = new <*>[{id: 75, level: 1, name: "Goujon"},
            {id: 71, level: 10, name: "Greuvette"},
            {id: 74, level: 20, name: "Truite"},
            {id: 77, level: 30, name: "Crabe Sourimi"},
            {id: 76, level: 40, name: "Poisson-Chaton"},
            {id: 78, level: 50, name: "Poisson Pané"},
            {id: 79, level: 60, name: "Carpe d'Iem"},
            {id: 81, level: 70, name: "Sardine Brillante"},
            {id: 263, level: 80, name: "Brochet"},
            {id: 264, level: 90, name: "Kralamoure"},
            {id: 265, level: 100, name: "Anguille"},
            {id: 266, level: 110, name: "Dorade Grise"},
            {id: 267, level: 120, name: "Perche"},
            {id: 268, level: 130, name: "Raie Bleue"},
            {id: 269, level: 140, name: "Lotte"},
            {id: 270, level: 150, name: "Requin Marteau-Faucille"},
            {id: 271, level: 160, name: "Bar Rikain"},
            {id: 272, level: 170, name: "Morue"},
            {id: 273, level: 180, name: "Tanche"},
            {id: 274, level: 190, name: "Espadon"},
            {id: 132, level: 200, name: "Poisskaille"}];

        public static function initialize():void {
            // Porcos to dragoeufs
            GatherUtils.MapDirection[72617985] = function():* {
                GatherUtils.MapDirection[72618497] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[72619009] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[72619521] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88080391] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[88211975] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211974] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211973] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211972] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211971] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211970] = DirectionsEnum.UP;
                GatherUtils.MapDirection[88211969] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[88212481] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84412416] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84412417] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84411905] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84411393] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84410881] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84410369] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84410370] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84410882] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84410883] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84410884] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84410885] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84410886] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411398] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84411397] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411909] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84411910] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84411911] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84411399] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84410887] = DirectionsEnum.LEFT;

                return DirectionsEnum.RIGHT;
            };
            // Dragoeufs to porcos
            GatherUtils.MapDirection[84410375] = function():* {
                GatherUtils.MapDirection[84410887] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411399] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411911] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84411910] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84411909] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84411397] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[84411398] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84410886] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84410885] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84410884] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[84410372] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84410371] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84410370] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84410369] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84410881] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411393] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84411905] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[84412417] = DirectionsEnum.UP;
                GatherUtils.MapDirection[84412416] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[88212481] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[88211969] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211970] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211971] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211972] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211973] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211974] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[88211975] = DirectionsEnum.RIGHT;
                GatherUtils.MapDirection[88080391] = DirectionsEnum.DOWN;
                GatherUtils.MapDirection[72619521] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[72619009] = DirectionsEnum.LEFT;
                GatherUtils.MapDirection[72618497] = DirectionsEnum.LEFT;

                return DirectionsEnum.RIGHT;
            };
            // Set as default path
            // GatherUtils.MapDirection[84410375](this);

            // Grobe peche
            GatherUtils.MapDirection[16690] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[17202] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[17201] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[17200] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[17712] = DirectionsEnum.UP;
            GatherUtils.MapDirection[17713] = DirectionsEnum.UP;
            GatherUtils.MapDirection[17714] = DirectionsEnum.UP;
            GatherUtils.MapDirection[17715] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[18227] = DirectionsEnum.UP;
            GatherUtils.MapDirection[18228] = DirectionsEnum.UP;
            GatherUtils.MapDirection[18229] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[17717] = DirectionsEnum.UP;
            GatherUtils.MapDirection[17718] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[17206] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[16694] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[16693] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[16181] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[16180] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[16692] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[16691] = DirectionsEnum.DOWN;

            // Amakna Alchi
            GatherUtils.MapDirection[88212746] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88212234] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212233] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88080649] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88081161] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88081160] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88080648] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88080647] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88081159] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88081671] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88081672] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88082184] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88082696] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88083208] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88083720] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083721] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083722] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083723] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88084235] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88084236] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88083724] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88083212] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88082700] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88082701] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88082702] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88082703] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88083215] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88083727] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083728] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083729] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88083730] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88083218] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88082706] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88082194] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88081682] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88081681] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88081169] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88081170] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88080658] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88212242] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212241] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212240] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212239] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212238] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88212750] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212749] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212748] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88212236] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88212235] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88212747] = DirectionsEnum.DOWN;

            // Ile de moon alchi
            GatherUtils.MapDirection[156762120] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156893704] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156894216] = 73317386;
            GatherUtils.MapDirection[156501510] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156500998] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156500997] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156500996] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156500995] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156500483] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156500484] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156499972] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156499971] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156499970] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156500482] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156500994] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156500993] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156500481] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156500480] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156500992] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156501504] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156501761] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156501249] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156501250] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156501762] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156502274] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156502786] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156893697] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156893698] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156894210] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156894211] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156894212] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156894724] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156894725] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156894726] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156894214] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156894215] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156893703] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156762119] = DirectionsEnum.DOWN;

            // Alchi otomai
            GatherUtils.MapDirection[63965441] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63965442] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63964930] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63964418] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63964419] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63964420] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63964421] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63963909] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63963908] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63963907] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63963395] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63963394] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63963393] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63963136] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[159765] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[160277] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[160278] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[159766] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[159254] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[158742] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[158230] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[157718] = DirectionsEnum.UP;
            GatherUtils.MapDirection[157717] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[157205] = alternate(157205, [DirectionsEnum.UP, DirectionsEnum.RIGHT]);

            // Chemin 1 
            GatherUtils.MapDirection[63965696] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63965184] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63964672] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63964929] = DirectionsEnum.RIGHT;

            // Chemin 2
            GatherUtils.MapDirection[156693] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156692] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[156180] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156179] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[155667] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155666] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155665] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155664] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155663] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155662] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155661] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155660] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156172] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[156173] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[156685] = DirectionsEnum.UP;
            GatherUtils.MapDirection[156684] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63965960] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63965959] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63965958] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[63966470] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63966469] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63965957] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[63965956] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63965444] = DirectionsEnum.UP;
            GatherUtils.MapDirection[63965445] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[63964933] = DirectionsEnum.LEFT;

            // Mine amakna
            GatherUtils.MapDirection[97259013] = alternate(97259013, [117984040, 117984043]);
            GatherUtils.MapDirection[97261061] = alternate(97261061, [117984050, 117984051, 117984056]);
            GatherUtils.MapDirection[97260037] = alternate(97260037, [117984108, 117984101, 117984114]);
            GatherUtils.MapDirection[97256967] = alternate(97256967, [117984098, 117984104]);
            GatherUtils.MapDirection[97260039] = alternate(97260039, [117984168, 117984169, 117984175]);
            GatherUtils.MapDirection[97257993] = alternate(97257993, [117983957, 117983963]);
            GatherUtils.MapDirection[97261065] = alternate(97261065, [117984012, 117984014, 117984015]);
            GatherUtils.MapDirection[97259019] = alternate(97259019, [117983987, 117983988]);
            GatherUtils.MapDirection[97255947] = alternate(97255947, [117984035, 117984039]);
            GatherUtils.MapDirection[97256971] = alternate(97256971, [117984072, 117984074, 117984078]);
            GatherUtils.MapDirection[97261063] = alternate(97261063, [117984105, 117984110, 117984112]);
            GatherUtils.MapDirection[97255945] = alternate(97255945, [117983950, 117983951, 117983956]);


            // Mine etain
            GatherUtils.MapDirection[178784264] = -1;

            // Paysan champs Bonta
            GatherUtils.MapDirection[142088205] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142088204] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142088203] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142088715] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142089227] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142089739] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142089738] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142089226] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142088714] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142088713] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142089225] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142089224] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142089736] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142090248] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142090247] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142089735] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142089223] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142088711] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142088710] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142088198] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142087686] = DirectionsEnum.UP;
            GatherUtils.MapDirection[142087685] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142087173] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087174] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087175] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142087687] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087688] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087689] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087690] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142087178] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142087179] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[142086667] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142086668] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[142086669] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142087181] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[142087693] = DirectionsEnum.RIGHT;

            // Chemin 3  (bucheron bois litneg)
            GatherUtils.MapDirection[155975694] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975693] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975692] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975691] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975690] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975689] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975688] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975687] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975686] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975685] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975684] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975683] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975682] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975681] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155975680] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[155976192] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976193] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976194] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976195] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976196] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976197] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[155976709] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976708] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976707] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976706] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976705] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976704] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[155977216] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977217] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977218] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[155977730] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977731] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[155977219] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977220] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977221] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977222] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977223] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977224] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977225] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977226] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977227] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155977228] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[155976716] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976715] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976714] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976713] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976712] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976711] = DirectionsEnum.UP;
            GatherUtils.MapDirection[155976710] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[155976198] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976199] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976200] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976201] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976202] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976203] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976204] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976205] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[155976206] = DirectionsEnum.LEFT;

            // Chemin 4  (bucheron rivage sufokia)
            GatherUtils.MapDirection[88087298] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087299] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087300] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087301] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087302] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087303] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087304] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087305] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087306] = DirectionsEnum.UP;
            GatherUtils.MapDirection[88087307] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88086795] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086794] = DirectionsEnum.LEFT;
            GatherUtils.MapDirection[88086282] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086281] = DirectionsEnum.RIGHT;
            GatherUtils.MapDirection[88086793] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086792] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086791] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086790] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086789] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086788] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086787] = DirectionsEnum.DOWN;
            GatherUtils.MapDirection[88086786] = DirectionsEnum.RIGHT;
        }

        private static function alternate(mapId:*, choices:*):Function {
            return function():* {
                var i:Number = 0;
                var roundCounter:* = GatherUtils.RoundCounter[mapId];
                if (roundCounter != null) {
                    i = roundCounter;
                }
                var ret_value:* = choices[i % choices.length];
                i++;
                GatherUtils.RoundCounter[mapId] = i;
                return ret_value;
            };
        }
    }
}
