package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.frames.PlugBotModule;
    import flash.events.TimerEvent;
    import flash.utils.*;
    import com.Utils;

    public class AutoFightModule extends PlugBotModule {
        public static var inFight:Boolean = false;
        private var enableAutoFight:Boolean = true;
        private var PA:int = 0;

        public function AutoFightModule() {
            super("AutoFight", "", "autofight", true);
        }

        public override function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    case packet is Definition.GameFightTurnStartPlayingMessage:
                        this.debug("GameFightTurnStartPlayingMessage");
                        AutoFightModule.inFight = true;
                        PA = Definition.CurrentPlayedFighterManager.getInstance().getCharacteristicsInformations().actionPointsCurrent;

                        var timer:* = new Timer(Utils.randomRange(800, 1800));
                        timer.addEventListener(TimerEvent.TIMER, function(e:TimerEvent):void {
                            var castableSpell:Function = getCastableSpell();

                            if (castableSpell != null) {
                                castableSpell();
                                return;
                            }
                            if (canMoveForward()) {
                                moveForward();
                                return;
                            }
                            castableSpell = getCastableSpell();
                            if (castableSpell != null) {
                                castableSpell();
                                return;
                            }

                            timer.stop();
                            e.currentTarget.removeEventListener(e.type, arguments.callee);
                            endTurn();
                        });
                        timer.start();

                        break;
                    case packet is Definition.GameFightJoinMessage:
                        AutoFightModule.inFight = true;
                        this.debug("GameFightJoinMessage");
                        Utils.randomDelayedCall(2000, 2500, function():void {
                            try {
                                Utils.logInConsole("randomDelayedCall");
                                var startFightMsg:* = new Definition.GameFightReadyMessage();
                                startFightMsg.initGameFightReadyMessage(true);
                                Definition.ConnectionsHandler.getConnection().send(startFightMsg);
                            } catch (e:*) {
                                Utils.logInConsole(e.getStackTrace());
                            }
                        });
                        break;
                    case packet is Definition.MapComplementaryInformationsDataMessage:
                        AutoFightModule.inFight = false;
                        break;
                    default:
                        return false;
                }
            } catch (e:*) {
                Utils.logInConsole("[AutoFightModule] : " + e.getStackTrace());
                return true;
            }
            return false;
        }

        private function getEnemyPositions():* {
            var myPosition:* = Definition.MapPoint.fromCellId(Definition.DofusEntities.getEntity(Definition.CurrentPlayedFighterManager.getInstance().currentFighterId).position.cellId);
            var results:Array = new Array();
            var testLos:Vector.<uint> = new Vector.<uint>();

            for each (var entity:* in Definition.FightEntitiesFrame.getCurrentInstance().getEntitiesDictionnary()) {
                if (entity.contextualId < 0 && entity is Definition.GameFightMonsterInformations) {
                    var monster:* = entity as Definition.GameFightMonsterInformations;
                    if (monster.spawnInfo.alive) {
                        var mp:* = Definition.MapPoint.fromCellId(entity.disposition.cellId);
                        results.push({"cellId": entity.disposition.cellId, "mp": mp, "dist": myPosition.distanceToCell(mp), "los": false});
                        testLos.push(entity.disposition.cellId);
                    }
                }
            }

            var testLosResult:* = Definition.LosDetector.getCell(Definition.DataMapProvider.getInstance(), testLos, myPosition);
            for each (var result:* in results) {
                if (testLosResult.indexOf(result.cellId) > -1) {
                    result.los = true;
                }
            }
            results.sortOn(["dist"], Array.NUMERIC);
            return results;
        }

        private function getCastableSpell():Function {
            if (!this.enableAutoFight || !AutoFightModule.inFight) {
                return null;
            }
            var enemyPositions:* = getEnemyPositions();
            this.debug("Remaining AP : " + this.PA);
            var spellId:int = getSpellIdFromBreed();
            for each (var spell:* in Definition.PlayedCharacterManager.getInstance().spellsInventory) {
                if (spell.spellId == spellId) {
                    for each (var enemyPosition:* in enemyPositions) {
                        if (enemyPosition.los && spell.apCost <= this.PA && spell.minRange <= enemyPosition.dist && spell.range >= enemyPosition.dist) {
                            PA -= spell.apCost;
                            return function() {
                                var gafcrmsg:* = new Definition.GameActionFightCastRequestMessage();
                                gafcrmsg.initGameActionFightCastRequestMessage(spellId, enemyPosition.cellId);
                                Definition.ConnectionsHandler.getConnection().send(gafcrmsg);
                            };
                        }
                    }
                }
            }
            var weapon:* = Definition.PlayedCharacterManager.getInstance().currentWeapon;
            for each (var enemyPosition:* in enemyPositions) {
                if (enemyPosition.los && weapon.apCost <= PA && weapon.minRange <= enemyPosition.dist && weapon.range >= enemyPosition.dist) {
                    PA -= weapon.apCost;
                    return function() {
                        Definition.Kernel.getWorker().process(Definition.GameFightSpellCastAction.create(0, 1));
                        triggerCellClick(enemyPosition.cellId);
                    };
                }
            }
            return null;
        }

        private function getSpellIdFromBreed():int {
            switch (Definition.PlayedCharacterManager.getInstance().infos.breed) {
                case Definition.BreedEnum.Roublard:
                    return 13475;
                case Definition.BreedEnum.Cra:
                    return 173;
                case Definition.BreedEnum.Ecaflip:
                    return 12846;
                default:
                    return -1;
            }
        }

        private function canMoveForward():Boolean {
            if (!this.enableAutoFight || !AutoFightModule.inFight) {
                return false;
            }
            var PM:* = Definition.CurrentPlayedFighterManager.getInstance().getCharacteristicsInformations().movementPointsCurrent;
            if (PM < 1)
                return false;
            var enemyPositions:* = getEnemyPositions();
            return enemyPositions.every(function(x:*) {
                return x.dist > 1
            });
        }

        private function moveForward():void {
            if (!this.enableAutoFight || !AutoFightModule.inFight) {
                return;
            }
            var enemyPositions:* = getEnemyPositions();
            var nearestEnemyPosition:* = enemyPositions[0];
            var PM:* = Definition.CurrentPlayedFighterManager.getInstance().getCharacteristicsInformations().movementPointsCurrent;
            var myCell:* = Definition.DofusEntities.getEntity(Definition.CurrentPlayedFighterManager.getInstance().currentFighterId).position.cellId;
            var path:* = Definition.Pathfinding.findPath(Definition.DataMapProvider.getInstance(), Definition.MapPoint.fromCellId(myCell), nearestEnemyPosition.mp, false);
            if (path.length > 1 && PM > 0) {
                var cellId:* = path.length >= PM ? path.getPointAtIndex(PM).cellId : path.getPointAtIndex(path.length - 1).cellId;
                triggerCellClick(cellId);
            }
        }

        private function triggerCellClick(cellId:*):void {
            var cellOverMessage:* = new Definition.CellOverMessage();
            var ccmsg:* = new Definition.CellClickMessage();
            var target:* = Definition.InteractiveCellManager.getInstance().getCell(cellId);
            ccmsg.cell = cellOverMessage.cell = Definition.MapPoint.fromCellId(cellId);
            ccmsg.cellId = cellOverMessage.cellId = cellId;
            ccmsg.cellContainer = cellOverMessage.cellContainer = target;
            ccmsg.cellDepth = cellOverMessage.cellDepth = target.parent.getChildIndex(target);
            Definition.Atouin.getInstance().handler.process(cellOverMessage);
            Definition.Atouin.getInstance().handler.process(ccmsg);
        }

        private function endTurn():void {
            if (!this.enableAutoFight || !AutoFightModule.inFight) {
                return;
            }
            this.debug("endTurn");
            var finDeTourMsg:* = new Definition.GameFightTurnFinishMessage();
            finDeTourMsg.initGameFightTurnFinishMessage();
            Definition.ConnectionsHandler.getConnection().send(finDeTourMsg);
        }

    }

}
