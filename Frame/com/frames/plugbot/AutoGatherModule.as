﻿package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.frames.PlugBotModule;
    import flash.events.TimerEvent;
    import com.Utils;
    import flash.utils.*;

    public class AutoGatherModule extends PlugBotModule {
        private static var stackFrame:* = null;
        private static var rpContextFrame:* = null;
        private static var rpWorldFrame:* = null;
        private var enableGathering:Boolean = false;
        private var autoClicker:Boolean = false;
        private var stackedInteractiveElemMessages:* = [];
        private var changeMapElems:* = [];
        private var gatherRessourcesIds:* = [];
        private var _elems:* = null;
        private var timer:Timer;

        public function AutoGatherModule() {
            super("AutoGather", "Automatically gather ressources", "gather", true);
        }

        public override function pushed():Boolean {
            GatherUtils.initialize();
            stackFrame = Definition.Kernel.getWorker().getFrame(Definition.StackManagementFrame) as Class;
            rpContextFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayContextFrame) as Class;
            rpWorldFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayWorldFrame) as Class;
            return true;
        }

        public override function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    case packet is Definition.AuthorizedCommandAction:
                        if (packet.command.indexOf("gather ") == 0) {
                            this.enableGathering = true;
                            Utils.logInConsole("AutoGathering + AutoFight enabled !");
                            var infos:* = packet.command.split(" ");
                            var metierVector:* = null;
                            switch (infos[1]) {
                                case "alchi":
                                    metierVector = GatherUtils.AlchimisteRessourceIds;
                                    break;
                                case "bucheron":
                                    metierVector = GatherUtils.BucheronRessourceIds;
                                    break;
                                case "mineur":
                                    metierVector = GatherUtils.MineurRessourceIds;
                                    break;
                                case "paysan":
                                    metierVector = GatherUtils.PaysansRessourceIds;
                                    break;
                                case "pecheur":
                                    metierVector = GatherUtils.PecheurRessourceIds;
                                    break;
                                default:
                                    Utils.logInConsole("metier non supporté : alchi, bucheron, mineur, paysan, pecheur de dispo");
                                    return true;
                            }
                            var minLevel:* = 1;
                            var maxLevel:* = 200;
                            var parsedMinLevel:* = Number(infos[2]);
                            if (parsedMinLevel) {
                                minLevel = parsedMinLevel;
                            }
                            var parsedMaxLevel:* = Number(infos[3]);
                            if (parsedMaxLevel) {
                                maxLevel = parsedMaxLevel;
                            }
                            var names:* = [];
                            for each (var ressource:* in metierVector) {
                                if (ressource.level >= minLevel && ressource.level <= maxLevel) {
                                    names.push(ressource.name);
                                    gatherRessourcesIds.push(ressource.id);
                                }
                            }
                            Utils.logInConsole("Type de ressource activé : " + names.join(", "));
                            nextAction();
                            return true;
                        }
                        if (packet.command == "stopgather") {
                            gatherRessourcesIds = [];
                            this.enableGathering = false;
                            Utils.logInConsole("AutoGathering + AutoFight disabled !");
                            return true;
                        }
                        if (packet.command == "state") {
                            Utils.logInConsole(this.enableGathering.toString());
                            return true;
                        }
                        if (packet.command == "autoclicker") {
                            autoClicker = !autoClicker;
                            Utils.logInConsole("Autoclicker set to : " + autoClicker.toString());
                            this.checkAutoclick();
                            return true;
                        }
                        if (packet.command == "autoclickerInfos") {
                            rpContextFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayContextFrame) as Definition.RoleplayContextFrame;
                            for each (var e:* in Definition.EntitiesManager.getInstance().entities) {
                                if (e is Definition.IInteractive && e is Definition.AnimatedCharacter && e.id < 0) {
                                    var infos:* = rpContextFrame.entitiesFrame.getEntityInfos(e.id) as Definition.GameRolePlayActorInformations;
                                    if (infos != null && infos is Definition.GameRolePlayGroupMonsterInformations && infos.staticInfos.underlings.length > 2) {
                                        Utils.logInConsole(e.id.toString());
                                        Utils.logInConsole(infos.toString());
                                    }
                                }
                            }
                            return true;
                        }
                        if (packet.command == "reverse") {
                            if (GatherUtils.MapDirection[72618497] == Definition.DirectionsEnum.RIGHT) {
                                GatherUtils.MapDirection[84410375](this);
                            } else {
                                GatherUtils.MapDirection[72617985](this);
                            }
                            return true;
                        }
                        break;
                    case packet is Definition.CurrentMapMessage:
                        if (this.autoClicker) {
                            this.delayedCheckAutoclick(450, 650);
                        }
                        break;
                    case packet is Definition.MapComplementaryInformationsDataMessage:
                        stackFrame = Definition.Kernel.getWorker().getFrame(Definition.StackManagementFrame) as Definition.StackManagementFrame;
                        rpContextFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayContextFrame) as Definition.RoleplayContextFrame;
                        rpWorldFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayWorldFrame) as Definition.RoleplayWorldFrame;
                        if (this.autoClicker) {
                            this.delayedCheckAutoclick(450, 650);
                        }
                        if (this.enableGathering) {
                            delayedNextAction();
                        }
                        break;
                    case packet is Definition.GameRolePlayShowActorMessage:
                        if (this.autoClicker) {
                            this.delayedCheckAutoclick(150, 250);
                        }
                        break;
                    case packet is Definition.InteractiveUsedMessage:
                        if (this.enableGathering && stackedInteractiveElemMessages != null && stackedInteractiveElemMessages.length > 0) {
                            timer = new Timer(Utils.randomRange(100, 150), stackedInteractiveElemMessages.length);
                            timer.addEventListener(TimerEvent.TIMER, unstackInteractiveElemMessages);
                            timer.start();
                        }
                        return false;
                    case packet is Definition.InteractiveUseEndedMessage:
                        if (this.enableGathering) {
                            nextAction();
                        }
                        return false;
                    case packet is Definition.InteractiveElementUpdatedMessage:
                        if (this.enableGathering) {
                            delayedNextAction();
                        }
                        return false;
                    case packet is Definition.NotificationUpdateFlagAction:
                        if (packet.index == 37) {
                            Utils.openNotification("Full pods");
                            this.enableGathering = false;
                            Utils.logInConsole("AutoGathering + AutoFight disabled !");
                        } else if (this.enableGathering) {
                            nextAction();
                        }
                        return false;
                    default:
                        return false;
                }
            } catch (e:*) {
                Utils.logInConsole("[AutoGatherModule] : " + e.getStackTrace());
                return true;
            }
            return false
        }

        // TODO : make it generic
        private function delayedNextAction():void {
            if (timer) {
                timer.stop();
            }
            timer = new Timer(Utils.randomRange(850, 1450), 1);
            timer.addEventListener(TimerEvent.TIMER, nextActionTimer);
            timer.start();
        }

        private function nextActionTimer(e:TimerEvent):void {
            this.nextAction();
        }

        private function nextAction():void {
            try {
                if (stackFrame == null) {
                    Utils.logInConsole("stackFrame is null");
                    stackFrame = Definition.Kernel.getWorker().getFrame(Definition.StackManagementFrame) as Class;
                }
                if (!this.gather() && stackFrame.stackOutputMessage.length == 0) {
                    changeMap();
                }
            } catch (e:*) {
                Utils.logInConsole("catched nextAction " + e.getStackTrace());
            }
        }

        private function gather():Boolean {
            try {
                if (!this.enableGathering) {
                    return false;
                }
                var availableElems:* = [];
                var nearestElem:* = null;
                var nearestElemDistance:* = null;
                stackedInteractiveElemMessages = [];
                var firstInteractiveElemMessage:* = null;
                changeMapElems = [];
                // Utils.logInConsole(rpContextFrame.entitiesFrame.interactiveElements.toString());
                _elems = rpContextFrame.entitiesFrame.interactiveElements;
                for (var i:* = 0; i < _elems.length; ) {
                    var elem:* = _elems[i];
                    if (elem.onCurrentMap && elem.enabledSkills.length > 0) {
                        if (elem.elementTypeId == -1 && elem.enabledSkills[0].skillInstanceUid != 65730394 && elem.enabledSkills[0].skillInstanceUid != 65730399 && elem.enabledSkills[0].skillInstanceUid != 65730404 && elem.enabledSkills[0].skillInstanceUid != 65730434 && elem.enabledSkills[0].skillInstanceUid != 65736184 && elem.enabledSkills[0].skillInstanceUid != 65742059 && elem.enabledSkills[0].skillInstanceUid != 65741975 && elem.enabledSkills[0].skillInstanceUid != 65742042 && elem.enabledSkills[0].skillInstanceUid != 65730450 && elem.enabledSkills[0].skillInstanceUid != 65730292 && elem.enabledSkills[0].skillInstanceUid != 65734310 && elem.enabledSkills[0].skillInstanceUid != 65734348 && elem.enabledSkills[0].skillInstanceUid != 65732939 && elem.enabledSkills[0].skillInstanceUid != 65732937 && elem.enabledSkills[0].skillInstanceUid != 65732852 && elem.enabledSkills[0].skillInstanceUid != 65732902 && elem.enabledSkills[0].skillInstanceUid != 65732940 && elem.enabledSkills[0].skillInstanceUid != 65733023 && elem.enabledSkills[0].skillInstanceUid != 65732968 && elem.enabledSkills[0].skillInstanceUid != 65730333) {
                            changeMapElems.push(i);
                        }
                        if (gatherRessourcesIds.indexOf(elem.elementTypeId) != -1 && elem.enabledSkills[0].skillInstanceUid != 65725652) {
                            var canBeAdded:* = true;
                            for (var j:* = 0; j < stackFrame.stackOutputMessage.length; j++) {
                                if (stackFrame.stackOutputMessage[j].interactiveElement.elementId == elem.elementId) {
                                    canBeAdded = false;
                                }
                            }
                            if (canBeAdded) {
                                availableElems.push(elem);
                            }
                        }
                    }
                    i++;
                }
                var tmp:* = Definition.EntitiesManager.getInstance().getEntity(Definition.PlayedCharacterManager.getInstance().id);
                if (tmp == null) {
                    return false;
                }
                var myCell:* = tmp.position.cellId;
                for (var i:* = 0; i < availableElems.length; i++) {
                    var elem:* = availableElems[i];
                    var position:* = Definition.Atouin.getInstance().getIdentifiedElementPosition(elem.elementId);
                    var path:* = Definition.Pathfinding.findPath(Definition.DataMapProvider.getInstance(), Definition.MapPoint.fromCellId(myCell), position);
                    if (nearestElem == null) {
                        nearestElem = elem;
                        nearestElemDistance = path.length;
                    } else {
                        if (path.length < nearestElemDistance) {
                            nearestElem = elem;
                            nearestElemDistance = path.length;
                        }
                    }
                }

                if (nearestElem != null) {
                    var position:* = Definition.Atouin.getInstance().getIdentifiedElementPosition(nearestElem.elementId);
                    if (firstInteractiveElemMessage == null) {
                        firstInteractiveElemMessage = new Definition.InteractiveElementActivationMessage(nearestElem, position, nearestElem.enabledSkills[0].skillInstanceUid);
                    } else {
                        stackedInteractiveElemMessages.push(new Definition.InteractiveElementActivationMessage(nearestElem, position, nearestElem.enabledSkills[0].skillInstanceUid));
                    }
                }
            } catch (e:*) {
                Utils.logInConsole("error catched in gather " + e.getStackTrace());
            }
            if (firstInteractiveElemMessage != null) {
                Definition.Kernel.getWorker().process(firstInteractiveElemMessage);
                return true;
            } else {
                return false;
            }
        }

        private function unstackInteractiveElemMessages(e:TimerEvent):void {
            Definition.Kernel.getWorker().process(stackedInteractiveElemMessages.shift());
        }


        private function changeMap():void {
            try {
                if (rpWorldFrame == null)
                    return;
                this.debug("changeMap");
                var tmp:* = GatherUtils.MapDirection[Definition.MapDisplayManager.getInstance().currentMapPoint.mapId];
                var dir:* = null;
                if (tmp is Function) {
                    dir = tmp(this);
                } else {
                    dir = tmp;
                }

                if (dir != null) {
                    if (dir > 7) {
                        // displayInChatIfDebug("InteractiveElementActivationMessage");
                        rpContextFrame = Definition.Kernel.getWorker().getFrame(Definition.RoleplayContextFrame) as Definition.RoleplayContextFrame;
                        _elems = rpContextFrame.entitiesFrame.interactiveElements;
                        for (var i:* = 0; i < _elems.length; ) {
                            var elem:* = _elems[i];
                            if (elem.enabledSkills.length > 0 && elem.enabledSkills[0].skillInstanceUid == dir) {
                                var msg:* = new Definition.InteractiveElementActivationMessage(elem, Definition.Atouin.getInstance().getIdentifiedElementPosition(elem.elementId), elem.enabledSkills[0].skillInstanceUid);
                                rpWorldFrame.process(msg);
                                break;
                            }
                            i++;
                        }
                    } else if (dir < 0) {
                        // Do nothing
                    } else {
                        // displayInChatIfDebug("adjacentMapClickMessage : " + dir.toString());
                        rpWorldFrame.process(adjacentMapClickMessage(dir));
                    }
                } else if (changeMapElems != null) {
                    // displayInChatIfDebug("changeMapElem");
                    var changeMapElem:* = _elems[changeMapElems[Math.floor(changeMapElems.length * Math.random())]];
                    var msg:* = new Definition.InteractiveElementActivationMessage(changeMapElem, Definition.Atouin.getInstance().getIdentifiedElementPosition(changeMapElem.elementId), changeMapElem.enabledSkills[0].skillInstanceUid);
                    rpWorldFrame.process(msg);
                }
            } catch (e:*) {
                Utils.logInConsole("changeMap : " + e.getStackTrace());
            }
        }

        private function adjacentMapClickMessage(dir:*):* {
            this.debug("adjacentMapClickMessage");
            var transitionCellId:* = this.getClosestTransitionCellId(dir);
            var adjacentMapClickMessage:* = new Definition.AdjacentMapClickMessage();
            var worldPoint:* = null;
            if (dir == Definition.DirectionsEnum.RIGHT) {
                adjacentMapClickMessage.adjacentMapId = Definition.MapDisplayManager.getInstance().getDataMapContainer().dataMap.rightNeighbourId;
            } else if (dir == Definition.DirectionsEnum.DOWN) {
                adjacentMapClickMessage.adjacentMapId = Definition.MapDisplayManager.getInstance().getDataMapContainer().dataMap.bottomNeighbourId;
            } else if (dir == Definition.DirectionsEnum.LEFT) {
                adjacentMapClickMessage.adjacentMapId = Definition.MapDisplayManager.getInstance().getDataMapContainer().dataMap.leftNeighbourId;
            } else if (dir == Definition.DirectionsEnum.UP) {
                adjacentMapClickMessage.adjacentMapId = Definition.MapDisplayManager.getInstance().getDataMapContainer().dataMap.topNeighbourId;
            }
            adjacentMapClickMessage.cellId = transitionCellId;
            if (dir == Definition.DirectionsEnum.UP && adjacentMapClickMessage.adjacentMapId == 84410369) {
                adjacentMapClickMessage.cellId = 12;
            }
            if (dir == Definition.DirectionsEnum.LEFT && adjacentMapClickMessage.adjacentMapId == 72619009) {
                adjacentMapClickMessage.cellId = 392;
            }
            if (dir == Definition.DirectionsEnum.RIGHT && adjacentMapClickMessage.adjacentMapId == 72619521) {
                adjacentMapClickMessage.cellId = 475;
            }
            if (dir == Definition.DirectionsEnum.RIGHT && adjacentMapClickMessage.adjacentMapId == 84411909) {
                adjacentMapClickMessage.cellId = 391;
            }
            adjacentMapClickMessage.fromAutotrip = false;
            return adjacentMapClickMessage;
        }

        private function getClosestTransitionCellId(direction:int):int {
            var cellId:int = 0;
            var dataMap:* = Definition.MapDisplayManager.getInstance().getDataMapContainer().dataMap;
            var playedEntity:* = Definition.DofusEntities.getEntity(Definition.PlayedCharacterManager.getInstance().id);
            var playedEntityCellId:uint = playedEntity.position.cellId;
            var myMapLinkedZone:int = dataMap.cells[playedEntityCellId].linkedZoneRP;
            var staticPoint:* = Definition.Cell.cellPixelCoords(playedEntityCellId);
            var playedEntityCellPoint:* = new Definition.Point(staticPoint.x, staticPoint.y);
            var closestCellForMapChange:Object = Definition.FrustumManager.getInstance().findNearestBorderCellFromPoint(direction, playedEntityCellPoint);
            cellId = closestCellForMapChange.cell;
            if (!Definition.CellUtil.isValidCellIndex(cellId)) {
                return -1;
            }
            var cell:* = dataMap.cells[cellId];
            if (!cell || !cell.mov || cell.linkedZoneRP != myMapLinkedZone) {
                return -1;
            }
            return cellId;
        }

        private function delayedCheckAutoclick(min:*, max:*):void {
            if (timer) {
                timer.stop();
            }
            timer = new Timer(Utils.randomRange(min, max), 1);
            timer.addEventListener(TimerEvent.TIMER, checkAutoclickTimer);
            timer.start();
        }

        private function checkAutoclickTimer(e:TimerEvent):void {
            this.checkAutoclick();
        }

        private function checkAutoclick():void {
            if (this.autoClicker) {
                var avaibleEntities:* = [];
                for each (var e:* in Definition.EntitiesManager.getInstance().entities) {
                    if (e is Definition.IInteractive && e is Definition.AnimatedCharacter && e.id < 0) {
                        var infos:* = rpContextFrame.entitiesFrame.getEntityInfos(e.id) as Definition.GameRolePlayActorInformations;
                        if (infos != null && infos is Definition.GameRolePlayGroupMonsterInformations && infos.staticInfos.underlings.length > 2) {
                            avaibleEntities.push(e);
                        }
                    }
                }
                var entity:* = avaibleEntities[Math.floor(avaibleEntities.length * Math.random())];
                if (!entity) {
                    return;
                }
                var clickMessage:* = new Definition.EntityClickMessage(entity);
                Definition.Kernel.getWorker().process(clickMessage);
            }
        }

    }

}
