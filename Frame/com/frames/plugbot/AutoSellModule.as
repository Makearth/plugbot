package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.frames.PlugBotModule;

    public class AutoSellModule extends PlugBotModule {
        protected static const _log:* = Definition.Log.getLogger(getQualifiedClassName(AutoSellFrame));
        private var autoSell:Boolean = false;
        private var adjust:Boolean = false;
        private var priceCache:Dictionary = new Dictionary();
        private var lastExchangeBidPriceForSellerMessage:* = null;
        private var _vendorObjects:Array = [];

        public function AutoSellModule() {
            super();
        }

        public function process(param1:Message):Boolean {
            var packet:* = param1;
            switch (true) {
                case packet is AuthorizedCommandAction:
                    if (packet.command == "autosell") {
                        autoSell = !autoSell;
                        displayInChat("Autosell set to : " + autoSell.toString());
                        return true;
                    }
                    if (packet.command == "adjust") {
                        autoSell = false;
                        adjust = true;
                        timer = new Timer(randomRange(300, 600), 1);
                        timer.addEventListener(TimerEvent.TIMER, triggerNextAdjust);
                        timer.start();
                        return true;
                    }
                    break;
                case packet is ExchangeBidPriceForSellerMessage:
                    lastExchangeBidPriceForSellerMessage = packet;
                    this.priceCache[packet.genericId] = packet;
                    displayInChatIfDebug("ExchangeBidPriceForSellerMessage : " + packet.genericId.toString() + " averagePrice : " + packet.averagePrice.toString() + " minimalPrices : " + packet.minimalPrices.toString());
                    if (autoSell) {
                        timer = new Timer(randomRange(300, 600), 1);
                        timer.addEventListener(TimerEvent.TIMER, triggerNextSell);
                        timer.start();
                    }
                    if (adjust) {
                        timer = new Timer(randomRange(300, 600), 1);
                        timer.addEventListener(TimerEvent.TIMER, triggerNextAdjust);
                        timer.start();
                    }
                    break;
                case packet is ExchangeStartedBidSellerMessage:
                    this.priceCache = new Dictionary()
                    this._vendorObjects = [];
                    for each (var objectInfo:* in packet.objectsInfos) {
                        this._vendorObjects.push(objectInfo);
                    }
                    break;
                case packet is ExchangeBidHouseItemRemoveOkMessage:
                    var comptSellItem:* = 0;
                    for each (var objectToSell:* in this._vendorObjects) {
                        if (objectToSell.objectUID == packet.sellerId) {
                            this._vendorObjects.splice(comptSellItem, 1);
                        }
                        comptSellItem++;
                    }
                    break;
                case packet is ExchangeBidHouseItemAddOkMessage:
                    displayInChat("ExchangeBidHouseItemAddOkMessage : unsoldDelay : " + packet.itemInfo.unsoldDelay.toString() + " objectUID : " + packet.itemInfo.objectUID.toString() + " quantity : " + packet.itemInfo.quantity.toString() + " objectPrice : " + packet.itemInfo.objectPrice.toString());
                    this._vendorObjects.push(packet.itemInfo);
                    break;
                default:
                    return false;
            }

        }

        private function triggerNextSell(e:TimerEvent):void {
            if (timer != null) {
                timer.stop();
            }
            if (autoSell && lastExchangeBidPriceForSellerMessage != null) {
                for each (var item:* in InventoryManager.getInstance().realInventory) {
                    if (lastExchangeBidPriceForSellerMessage.genericId == item.objectGID) {
                        var quantity:uint = 0;
                        var bestPrice:uint = 0;
                        if (item.quantity >= 100) {
                            quantity = 100;
                            bestPrice = lastExchangeBidPriceForSellerMessage.minimalPrices[2];
                        } else if (item.quantity >= 10) {
                            quantity = 10;
                            bestPrice = lastExchangeBidPriceForSellerMessage.minimalPrices[1];
                        } else if (item.quantity >= 1) {
                            quantity = 1;
                            bestPrice = lastExchangeBidPriceForSellerMessage.minimalPrices[0];
                        }
                        if (bestPrice > 0) {
                            Kernel.getWorker().process(ExchangeShopStockMouvmentAddAction.create(item.objectUID, quantity, bestPrice));
                            if (item.quantity - quantity > 0) {
                                timer = new Timer(randomRange(300, 600), 1);
                                timer.addEventListener(TimerEvent.TIMER, triggerNextSell);
                                timer.start();
                            } else {
                                lastExchangeBidPriceForSellerMessage = null;
                            }
                            return;
                        } else {
                            displayInChat("Can't sell, no best price");
                            return;
                        }
                    }
                }
            }
        }

        private function triggerNextAdjust(e:TimerEvent):void {
            if (adjust) {
                if (timer != null) {
                    timer.stop();
                }
                for each (var obj:* in this._vendorObjects) {
                    var price:* = this.priceCache[obj.objectGID];
                    if (price != null) {
                        var index:uint = 2;
                        if (obj.quantity >= 100) {
                            index = 2;
                        } else if (obj.quantity >= 10) {
                            index = 1;
                        } else if (obj.quantity >= 1) {
                            index = 0;
                        }

                        var bestPrice = price.minimalPrices[index];
                        var myObjPrice = obj.objectPrice;
                        if (bestPrice < myObjPrice) {
                            displayInChat("Adjust : " + obj.objectUID.toString() + " from " + myObjPrice.toString() + " to " + bestPrice.toString());
                            Kernel.getWorker().process(ExchangeObjectModifyPricedAction.create(obj.objectUID, obj.quantity, bestPrice));
                            timer = new Timer(randomRange(300, 600), 1);
                            timer.addEventListener(TimerEvent.TIMER, triggerNextAdjust);
                            timer.start();
                            return;
                        }
                    } else {
                        Kernel.getWorker().process(ExchangeBidHousePriceAction.create(obj.objectGID));
                        return;
                    }
                }
                adjust = false;
            }
        }

    }

}
