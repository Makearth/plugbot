﻿package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.ankamagames.jerakine.network.NetworkMessage;
    import com.Definition;
    import flash.utils.getQualifiedClassName;
    import flash.utils.*;
    import flash.events.TimerEvent;
    import com.Utils;
    import com.frames.PlugBotModule;

    public class DebugModule extends PlugBotModule {
        private var fulldebug:Boolean = false;
        private var _elems:* = null;
        private var _dataApi:* = new Definition.DataApi();

        public function DebugModule() {
            super("Debug", "Debug module to inspect messages and test functions", "debug", false);
        }

        public override function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    case packet is Definition.AuthorizedCommandAction:
                        if (packet.command == "network") {
                            this.debugMode = !this.debugMode;
                            return true;
                        }
                        if (packet.command == "internal") {
                            fulldebug = !fulldebug;
                            return true;
                        }

                        if (packet.command == "breed") {
                            log(Definition.PlayedCharacterManager.getInstance().infos.breed.toString());
                            return true;
                        }
                        if (packet.command == "infos") {
                            var rpContextFrame:* = Definition.Kernel.getWorker().getFrame(Definition.RoleplayContextFrame);
                            _elems = rpContextFrame.entitiesFrame.interactiveElements;
                            for (var i:* = 0; i < _elems.length; ) {
                                var elem:* = _elems[i];
                                var ie:* = Definition.Atouin.getInstance().getIdentifiedElement(elem.elementId);
                                var position:* = Definition.Atouin.getInstance().getIdentifiedElementPosition(elem.elementId);
                                // var worldObject:* = ie as SpriteWrapper;
                                // entity = worldObject.getChildAt(0) as WorldEntitySprite;
                                if (elem.enabledSkills.length > 0) {
                                    log(elem.elementTypeId);
                                    log(elem.enabledSkills[0].skillInstanceUid.toString());
                                    log(position.toString());
                                }
                                i++;
                            }
                            return true;
                        }
                        if (packet.command.indexOf("popup ") == 0) {
                            Utils.openPopup(packet.command.split(" ")[1]);
                            return true;
                        }
                        if (packet.command.indexOf("log ") == 0) {
                            Utils.logInConsole(packet.command.split(" ")[1]);
                            return true;
                        }
                        if (packet.command.indexOf("notification ") == 0) {
                            Utils.openNotification(packet.command.split(" ")[1]);
                            return true;
                        }
                        if (packet.command == "getallzaap") {
                            Utils.logInConsole(_dataApi.getAllZaaps());
                            return true;
                        }
                        if (packet.command == "testdelay") {
                            Utils.randomDelayedCall(2000, 2500, function():void {
                                Utils.openPopup("Delay");
                            });
                            return true;
                        }

                        break;
                    case packet is Definition.SaveMessageAction:
                    case packet is Definition.FocusChangeMessage:
                    case packet is Definition.ChangeMessage:
                    case packet is Definition.MapsLoadingStartedMessage:
                    case packet is Definition.MouseMessage:
                    case packet is Definition.KeyboardMessage:
                    case packet is Definition.MapRenderProgressMessage:
                        return false;
                    case packet is Definition.AdjacentMapClickMessage:
                        log("AdjacentMapClickMessage : " + packet.adjacentMapId.toString() + " " + packet.cellId.toString());
                        return false;
                    case packet is Definition.InteractiveElementActivationMessage:
                        log("InteractiveElementActivationMessage : " + packet.skillInstanceId);
                        return false;
                    case packet is Definition.GameActionFightSpellCastMessage:
                        log("SPELL CAST : " + packet.spellId.toString());
                        break;
                    case packet is Definition.GameFightSpellCastAction:
                        log("SPELL CAST ACTION : " + packet.spellId.toString() + " SLOT : " + packet.slot.toString());
                        break;
                    case packet is Definition.ExchangeBidHousePriceAction:
                        // log("ExchangeBidHousePriceAction : genId : " + packet.genId.toString());
                        break;
                    case packet is NetworkMessage:
                        var qualifiedName:String = getQualifiedClassName(param1);
                        if (debugMode && qualifiedName.indexOf("ChatServerMessage") == -1) {
                            log(qualifiedName);
                        }
                        break;
                    case packet is Definition.CellOverMessage:
                        // log("CellOverMessage : " + packet.cellId.toString());
                        break;
                    case packet is Definition.CellClickMessage:
                        log("CellClickMessage cellId : " + packet.cellId.toString());
                        log("CellClickMessage cell : " + packet.cell.toString());
                        log("CellClickMessage cellDepth : " + packet.cellDepth.toString());
                        log("CellClickMessage cellContainer : " + packet.cellContainer.toString());
                        break;

                    default:
                        if (fulldebug) {
                            var qualifiedName:String = getQualifiedClassName(param1);
                            if (qualifiedName.indexOf("Over") == -1 && qualifiedName.indexOf("Out") == -1 && qualifiedName.indexOf("Wheel") == -1) {
                                log(qualifiedName);
                            }
                        }
                        break;
                }
            } catch (e:*) {
                log("catched globally " + e.getStackTrace());
                return true;
            }
            return false;
        }

        private function log(message:String):void {
            Utils.logInConsole(message);
        }

    }

}
