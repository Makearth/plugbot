package com.frames.plugbot {
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.frames.PlugBotModule;
    import com.Utils;

    public class ArchimonstreFinderModule extends PlugBotModule {
        public function ArchimonstreFinderModule() {
            super("ArchimonstreFinder", "Popup a notification if an archimonstre is on the map", "archi", false);
        }

        public override function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    // TODO
                    /* var infos:* = this.rpContextFrame.entitiesFrame.getEntityInfos(e.id) as GameRolePlayActorInformations;
                       if (infos is GameRolePlayGroupMonsterInformations) {
                       if (Monster.getMonsterById(infos.staticInfos.mainCreatureLightInfos.creatureGenericId).isMiniBoss) {
                       KernelEventsManager.getInstance().processCallback(HookList.ExternalNotification,3,["ARCHIMONSTRE !!!"]);
                       }
                       if(infos.staticInfos.underlings && infos.staticInfos.underlings.length > 0)
                       {
                       for each(var migi:* in infos.staticInfos.underlings)
                       {
                       if(Monster.getMonsterById(migi.creatureGenericId).isMiniBoss)
                       {
                       KernelEventsManager.getInstance().processCallback(HookList.ExternalNotification,3,["ARCHIMONSTRE !!!"]);
                       }
                       }
                       }
                       }*/
                    default:
                        return false;
                }
            } catch (e:*) {
                Utils.logInConsole("[ArchimonstreFinderFrame] : " + e.getStackTrace());
            }
            return false;
        }

    }

}
