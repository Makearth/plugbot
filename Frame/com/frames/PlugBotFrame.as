﻿package com.frames {
    import com.ankamagames.jerakine.messages.Frame;
    import com.ankamagames.jerakine.messages.Message;
    import flash.utils.getDefinitionByName;
    import flash.utils.getQualifiedClassName;
    import com.ankamagames.jerakine.network.NetworkMessage;
    import com.ankamagames.jerakine.utils.errors.SingletonError;
    import flash.system.ApplicationDomain;
    import flash.system.SecurityDomain;
    import flash.display.Loader;
    import flash.system.LoaderContext;
    import flash.events.ErrorEvent;
    import flash.utils.*;
    import flash.events.TimerEvent;
    import com.Definition;
    import com.Utils;
    import flash.events.*;
    import flash.net.*;
    import flash.filesystem.*;
    import com.ankamagames.dofus.network.messages.security.RawDataMessage;
    import com.frames.plugbot.*;

    public class PlugBotFrame implements Frame {
        private var timer:Timer;
        private var rawDataFrame:* = new RawDataMessageFrame();
        private var modules:Vector.<PlugBotModule> = new Vector.<PlugBotModule>();

        public function PlugBotFrame() {
        }

        public function pushed():Boolean {
            Utils.displayInChat("Welcome in PlugBot®");
            Definition.Kernel.getWorker().addFrame(this.rawDataFrame);
            modules.push(new ArchimonstreFinderModule());
            modules.push(new AutoFightModule());
            modules.push(new AutoGatherModule());
            modules.push(new BoilerplateModule());
            modules.push(new DebugModule());
            return true;
        }

        public function pulled():Boolean {
            return true;
        }

        public function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    case packet is Definition.IdentificationSuccessMessage:
                        packet.hasRights = true;
                        packet.hasConsoleRight = true;
                        // Definition.ModuleDebugManager.display(true);
                        Definition.KernelEventsManager.getInstance().processCallback(Definition.HookList.ToggleConsole);
                        return false;
                    case packet is Definition.ChatCommandAction:
                        if (packet.command == "console") {
                            Definition.KernelEventsManager.getInstance().processCallback(Definition.HookList.ToggleConsole);
                            return true;
                        }
                        break;
                    case packet is Definition.PopupWarningMessage:
                        Utils.openNotification("MODO !!!");
                        timer = new Timer(Utils.randomRange(3000, 4500), 1);
                        timer.addEventListener(TimerEvent.TIMER, questionMarkInGeneral);
                        timer.start();
                        for each (var module:PlugBotModule in modules) {
                            module.enabled = false;
                        }
                        return false;
                    case packet is Definition.AuthorizedCommandAction:
                        if (packet.command.indexOf("load ") == 0) {
                            var swfFile:File = new File(packet.command.split(" ")[1]);
                            if (swfFile.exists) {
                                var data:* = new ByteArray();
                                var stream:FileStream = new FileStream();
                                stream.open(swfFile, FileMode.READ);
                                stream.readBytes(data);
                                stream.close();
                                for each (var module:PlugBotModule in modules) {
                                    module.enabled = false;
                                }
                                Definition.Kernel.getWorker().removeFrame(this.rawDataFrame);
                                var rdm:* = new RawDataMessage();
                                rdm.initRawDataMessage(data);
                                Definition.Kernel.getWorker().process(rdm);
                                Definition.Kernel.getWorker().removeFrame(this);
                                Utils.logInConsole("Reloaded");
                            } else {
                                Utils.logInConsole("File not found");
                            }
                            return true;
                        }
                        if (packet.command == "help" || packet.command.indexOf("module") > 0) {
                            this.getModuleDetails();
                            return true;
                        }

                        for each (var module:PlugBotModule in modules) {
                            if (packet.command == module.toggleCmd) {
                                module.enabled = !module.enabled;
                                Utils.logInConsole(module.name + " " + (module.enabled ? "enabled" : "disabled") + " !");
                                this.getModuleDetails();
                                return true;
                            }
                            if (packet.command == module.toggleCmd + " debug") {
                                module.debugMode = !module.debugMode;
                                Utils.logInConsole("Debug for " + module.name + " " + (module.debugMode ? "enabled" : "disabled") + " !");
                                this.getModuleDetails();
                                return true;
                            }
                        }

                        if (packet.command.indexOf("isFrameLoaded ") > 0) {
                            var definition:* = getDefinitionByName(packet.command.split(" ")[1]);
                            Utils.logInConsole(definition.toString() + " : isLoaded = " + Definition.Kernel.getWorker().contains(definition));
                        }

                        return false;
                }
            } catch (e:*) {
                Utils.logInConsole("[PlugBotFrame] : " + e.getStackTrace());
                return true;
            }
            return false;
        }

        private function getModuleDetails():void {
            for each (var module:PlugBotModule in modules) {
                Utils.logInConsole(module.getDetail());
            }
        }

        private function questionMarkInGeneral(e:TimerEvent):void {
            var ccmm:* = new Definition.ChatClientMultiMessage();
            ccmm.initChatClientMultiMessage("?", 0);
            Definition.ConnectionsHandler.getConnection().send(ccmm);
        }

        public function get priority():int {
            return 2;
        }
    }

}
