﻿package com.frames {
    import flash.display.Sprite;
    import flash.utils.*;
    import flash.net.*;
    import __AS3__.vec.*;
    import com.ankamagames.jerakine.messages.Frame;
    import com.ankamagames.dofus.network.messages.security.RawDataMessage;
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.Utils;

    public class RawDataMessageFrame implements Frame {
        protected static const _log:* = Definition.Log.getLogger(getQualifiedClassName(RawDataMessageFrame));
        private static var Signature:* = Definition.Signature;
        private static var SIGNATURE_KEY_V1:* = Definition.SIGNATURE_KEY_V1;
        private static var SIGNATURE_KEY_V2:* = Definition.SIGNATURE_KEY_V2;

        public function RawDataMessageFrame() {
            super();
        }

        public function pushed():Boolean {
            return true;
        }

        public function pulled():Boolean {
            return true;
        }

        public function process(param1:Message):Boolean {
            var packet:* = param1;
            switch (true) {
                case packet is RawDataMessage:
                    var content:* = new ByteArray();
                    var signature:* = new Signature(SIGNATURE_KEY_V1, SIGNATURE_KEY_V2);
                    _log.info("[custom] Bytecode len: " + packet.content.length + ", hash: " + Definition.MD5.hashBytes(packet.content));
                    packet.content.position = 0;
                    if (signature.verify(packet.content, content)) {
                        Utils.loadByteArray(content);
                    } else {
                        _log.error("Signature incorrecte");
                    }
                    return true;
                default:
                    return false;
            }

        }

        public function get priority():int {
            return 3;
        }


    }

}
