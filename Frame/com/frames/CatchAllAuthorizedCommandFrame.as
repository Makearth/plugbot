﻿package com.frames {
    import flash.display.Sprite;
    import flash.utils.*;
    import flash.net.*;
    import __AS3__.vec.*;
    import com.ankamagames.jerakine.messages.Frame;
    import com.ankamagames.dofus.network.messages.security.RawDataMessage;
    import com.ankamagames.jerakine.messages.Message;
    import com.Definition;
    import com.Utils;

    public class CatchAllAuthorizedCommandFrame implements Frame {

        public function CatchAllAuthorizedCommandFrame() {
            super();
        }

        public function pushed():Boolean {
            return true;
        }

        public function pulled():Boolean {
            return true;
        }

        public function process(param1:Message):Boolean {
            try {
                var packet:* = param1;
                switch (true) {
                    case packet is Definition.AuthorizedCommandAction:
                        Utils.logInConsole("Unhandled authorized command : " + packet.command);
                        return true;
                    default:
                        return false;
                }
            } catch (e:*) {
                Utils.logInConsole("[CatchAllAuthorizedCommandFrame] : " + e.getStackTrace());
				return true;
            }
			return false;
        }

        public function get priority():int {
            return 0;
        }

    }

}
