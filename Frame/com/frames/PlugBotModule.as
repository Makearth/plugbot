﻿package com.frames {
    import com.ankamagames.jerakine.messages.Message;
    import com.ankamagames.jerakine.messages.Frame;
    import com.Definition;
    import com.Utils;

    public class PlugBotModule implements Frame {
        public var name:String;
        public var description:String;
        public var toggleCmd:String;
        public var debugMode:Boolean = false;
        public var _enabled:Boolean = false;

        public function get enabled():Boolean {
            return _enabled;
        }

        public function set enabled(enabled:Boolean):void {
            _enabled = enabled;
            if (enabled)
                Definition.Kernel.getWorker().addFrame(this);
            else
                Definition.Kernel.getWorker().removeFrame(this);
        }

        public virtual function pushed():Boolean {
            return true;
        }

        public virtual function pulled():Boolean {
            return true;
        }

        public virtual function process(param1:Message):Boolean {
            return false;
        }

        public function PlugBotModule(name:String, description:String, toggleCmd:String, enabled:Boolean) {
            this.name = name;
            this.description = description;
            this.toggleCmd = toggleCmd;
            this.enabled = enabled;
        }

        public function get priority():int {
            return 1;
        }

        public function getDetail():String {
            return "[" + (this.enabled ? "X" : " ") + "]" + (this.debugMode ? " (DEBUG) " : "") + this.name + ": - /" + this.toggleCmd + " : " + this.description;
        }

        protected function debug(message:String):void {
            Utils.logInConsole(message);
        }
    }
}
