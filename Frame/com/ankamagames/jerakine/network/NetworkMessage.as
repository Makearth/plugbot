﻿package com.ankamagames.jerakine.network
{
   import flash.utils.IDataOutput;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;


   public class NetworkMessage extends Object implements INetworkMessage
   {

      public function NetworkMessage() {
         super();
      }

      public static function writePacket(param1:IDataOutput, param2:int, param3:ByteArray) : void {
      }

      public function get isInitialized() : Boolean {
         return false;
      }

      public function reset() : void {
      }

      public function getMessageId() : uint {
         return 0;
      }

      public function pack(param1:IDataOutput) : void {
      }

      public function unpack(param1:IDataInput, param2:uint) : void {
      }
   }

}