﻿package com.ankamagames.dofus.network.messages.game.interactive
{
   import com.ankamagames.jerakine.network.NetworkMessage;
   import com.ankamagames.jerakine.network.INetworkMessage;
   import flash.utils.IDataOutput;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;


   public class InteractiveUseRequestMessage extends NetworkMessage implements INetworkMessage
   {
          
      public function InteractiveUseRequestMessage() {
         super();
      }

      public static const protocolId:uint = 5001;

      private var _isInitialized:Boolean = false;

      override public function get isInitialized() : Boolean {
         return false;
      }

      public var elemId:uint = 0;

      public var skillInstanceUid:uint = 0;

      override public function getMessageId() : uint {
         return 5001;
      }

      public function initInteractiveUseRequestMessage(param1:uint=0, param2:uint=0) : InteractiveUseRequestMessage {
         return this;
      }

      override public function reset() : void {
      }

      override public function pack(param1:IDataOutput) : void {
      }

      override public function unpack(param1:IDataInput, param2:uint) : void {
      }

      public function serialize(param1:IDataOutput) : void {
      }

      public function serializeAs_InteractiveUseRequestMessage(param1:IDataOutput) : void {
      }

      public function deserialize(param1:IDataInput) : void {
      }

      public function deserializeAs_InteractiveUseRequestMessage(param1:IDataInput) : void {
      }
   }

}