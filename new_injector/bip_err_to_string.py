from bip import *
import struct 

error_codes = {}
error_strs = {}
def build_dict(errorcodes_addr, nb_errorcodes = 0x21A):
    i = 0

    errorcodes = BipData.get_bytes(errorcodes_addr, 0x21A*8)

    while i < nb_errorcodes:
        err,idx = struct.unpack("II", errorcodes[i*8:(i*8)+8])
        error_codes[err] = idx
        i += 1

    strings = BipData.get_bytes(errorcodes_addr + 0x21A*8, 0x21A*4)
    for i in range(nb_errorcodes):
        error_strs[i] = struct.unpack("I", strings[i*4:(i*4)+4])[0]

def err_to_str(err):
    idx = error_codes[err]
    return BipData.get_cstring(error_strs[idx])

def idx_to_str(idx):
    return error_strs[idx]