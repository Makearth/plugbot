'use strict';

const Protocol = require('./libs/protocol');
const ByteArray = require('./libs/bytearray');
const swf_patcher = require('./frida_memory_inject.js');
var plugbot;

recv("swf", function onMessage(swf_message) {
	console.log("received swf message, replacing swf payload");
	console.log(swf_message)
	console.log(swf_message.payload)
	plugbot = swf_message.payload
	send("Received swf data, all good !");
})

Interceptor.detachAll();

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

async function do_replace() {
	console.log("starting replace...")
	let i = 0;
	while (i < 10) {
		swf_patcher.match_and_replace(swf_patcher.my_needle, swf_patcher.my_haystack_string, false)
		await sleep(100)
		i++;
	}
}

do_replace()

var recv_it = Interceptor.attach(Module.getExportByName(null, "recv"), {
	onEnter(args) {
		this.fd = args[0];
		this.buf = ptr(args[1]);
		this.size = args[2].toInt32();
	},

	onLeave(ret) {
		let ret_value = ret.toInt32();

		if (ret_value > 2) {
			let nat_ptr = ptr(this.buf);
			let bArray = nat_ptr.readByteArray(ret_value);
			const data = Buffer.from(bArray, "binary");
			console.log(bArray);
			const message = Protocol.MessageReceiver.parse(new Protocol.CustomDataWrapper(data));
			if (message)
				console.log(`${message.constructor.name} received`);
			if (message instanceof Protocol.ProtocolRequired) {
				var rawDataMessage = new Protocol.RawDataMessage();
				rawDataMessage.content = plugbot;
				var output = new ByteArray();
				rawDataMessage.pack(new Protocol.CustomDataWrapper(output));
				nat_ptr.add(ret_value).writeByteArray(output.buffer);
				ret.replace(ret_value + output.buffer.length);
				console.log("INJECTION DONE !");
				recv_it.detach();
			}
		}
	}
}
);