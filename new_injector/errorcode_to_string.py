import struct

# domainapplication_load_bytes__ !

f = open(r"D:\DofusBot\bins\v2\export_results.bin", "rb")


error_codes = {}
for i in range(0x21A):
    error_code, idx = struct.unpack("II", f.read(8))
    print("registering errorcode 0x%X with idx 0x%X" % (error_code, idx))
    error_codes[error_code] = idx


error_strs = {}
for i in range(0x21A):
    error_str_addr = f.read(4)
    error_strs[i] = struct.unpack("I", error_str_addr)[0]

def err_to_str(err):
    idx = error_codes[err]
    return error_strs[idx]

def idx_to_str(idx):
    return error_strs[idx]