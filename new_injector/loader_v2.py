# python loader.py

import frida
import time
import os

def on_message(message, data):
    print(message)

device = frida.get_local_device()
pid = device.spawn(r"%s\Ankama\zaap\dofus\Dofus.exe" % os.getenv('LOCALAPPDATA'))

f = None
try:
    f = open('plugbot.swf', 'rb')
    swf_data = f.read()
finally:
    if f is not None:
       f.close()

device.resume(pid)
time.sleep(0.5)

session = device.attach(pid)

print("attaching mitm script...")
mitm_script = session.create_script(open(r".\test_direct_call.js").read())
mitm_script.on('message', on_message)
mitm_script.load()

mitm_script.post({"type": "swf", "payload": [i for i in swf_data]})



print("done !")
input()
	