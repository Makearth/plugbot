// python C:\Python38\Lib\site-packages\frida_tools\repl.py -l C:\Users\tresa\Documents\PlugBot\new_injector\frida_memory_inject.js Dofus.exe

function toHexString(byteArray) {
	return Array.from(byteArray, function (byte) {
		return ('0' + (byte & 0xFF).toString(16)).slice(-2);
	}).join(' ')
}

var last_needle = null;

function match_and_replace(needle, haystack_string, quiet) {
	let haystack = new Uint8Array(needle.split(" ").length);
	haystack.fill(0x2);


	function log(msg) {
		if (quiet == false) {
			console.log(msg);
		}
	};

	if (haystack_string != "nope") {
		log("Gonna replace with " + haystack_string);
		let haystack_bytes = haystack_string.split(" ");
		for (let i = 0; i < haystack_bytes.length; ++i)
			haystack[i] = parseInt(haystack_bytes[i], 16);
	}
	else {
		log("Gonna replace with NOP");
	}

	log("Scanning to replace...");
	Process.enumerateRanges("rw-").forEach(x => {
		Memory.scan(x.base, x.size, needle, {
			onMatch(address, size) {
				console.log("match on  " + address + " with size " + size + " and protection " + x.protection + " and file " + x.file);
				var nat_ptr = ptr(address);

				console.log(hexdump(nat_ptr, {
					offset: 0,
					length: (size + 0x20) & 0xfffff0,
					header: true,
					ansi: true
				}));

				if (haystack.length != 0) {
					last_needle = toHexString(haystack);
					nat_ptr.writeByteArray(haystack);
					console.log("replaced by payload of size " + haystack.length);
					console.log(hexdump(nat_ptr, {
						offset: 0,
						length: (size + 0x20) & 0xfffff0,
						header: true,
						ansi: true
					}));
				}
			},
			onComplete() {

			}
		}

		)
	})
	log("Scan done");
}

function loop_match_and_replace(_needle, _haystack, interval = 500) {
	console.log("Starting match and replace loop for " + _needle + " and " + _haystack)
	return setInterval(match_and_replace, interval, _needle, _haystack, true)
}

// works !
var my_needle = "f0 4c 60 5a 46 e2 f3 02 00 60 e3 4e 46 ec 1b 01 12 12 00 00 f0 4e 60 e0 ff 02 2c f7 90 07 4f f6 1c 01 f0 4f 27 48 f0 52 5d 01 4a 01 00 80 01 d7 f0 53 5d ea 46 60 dd ff 02 60 df ff 02 4a ea 46 02 80 ea 46 63 04 f0 55 60 e0 ff 02 2c f8 90 07 d2 66 85 2f 66 dc 31 a0 2c f9 90 07 a0 60 8b 45 d2 66 85 2f 46 e0 81 03 01 a0 4f f4 1c 01 f0 57 d2 66 85 2f 24 00 61 80 37 f0 58 62 04 d2 66 85 2f d3 46 88 23 02 12 73 00 00 ef 01 da a6 06 05 5a ef 01 fa 90 07 06 5c f0 5a 5d fe 18 4a fe 18 00 80 fe 18 63 06 f0 5b 62 06 66 ec ea 03 60 ed ea 03 66 ee ea 03 60 d5 24 66 8b f6 02 27 24 00 26 4f cf 1a 05 f0 5c 5d 71 27 5d 8e 03 60 8e 03 66 b9 de 03 4a 8e 03 01 4a 71 02 80 71 63 07 f0 5d 60 c6 8c 01 62 07 26 4f 9d a1 03 02 f0 5e 62 06 d3 62 07 4f ef ea 03 02";
let my_haystack_string = "f0 52 5d 01 4a 01 00 80 01 d7 f0 53 5d ea 46 60 dd ff 02 60 df ff 02 4a ea 46 02 80 ea 46 63 04 f0 55 60 e0 ff 02 2c f8 90 07 d2 66 85 2f 66 dc 31 a0 2c f9 90 07 a0 60 8b 45 d2 66 85 2f 46 e0 81 03 01 a0 4f f4 1c 01 f0 57 d2 66 85 2f 24 00 61 80 37 f0 5a 5d fe 18 4a fe 18 00 80 fe 18 63 06 f0 5b 62 06 66 ec ea 03 60 ed ea 03 66 ee ea 03 60 d5 24 66 8b f6 02 27 24 00 26 4f cf 1a 05 f0 5c 5d 71 27 5d 8e 03 60 8e 03 66 b9 de 03 4a 8e 03 01 4a 71 02 80 71 63 07 f0 5d 60 c6 8c 01 62 07 26 4f 9d a1 03 02 f0 5e 62 06 d2 66 85 2f 62 07 4f ef ea 03 02";


exports.my_needle = my_needle;
exports.my_haystack_string = my_haystack_string;
exports.loop_match_and_replace = loop_match_and_replace;
exports.match_and_replace = match_and_replace;