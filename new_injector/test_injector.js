

let adobe_air_mod = Process.findModuleByName("Adobe AIR.dll")

var dll_base = ptr(adobe_air_mod.base)


var target_func = dll_base.add(0x2d543e)

console.log(hexdump(target_func, {
    offset: 0,
    length: 0x100,
    header: true,
    ansi: false
}))

console.log("Attaching...")
var it = Interceptor.attach(target_func, {
	onEnter(args) {
        console.log("ENTERED FUNCTION LOAD BYTES !!")

        var func_this = this.context.ecx

        var byte_array = args[1]
        console.log("byte_array == ", byte_array)

        console.log(hexdump(byte_array, {
            offset: 0,
            length: 0x100,
            header: true,
            ansi: false
        }))

        var ebp = this.context.ebp;
        console.log("ebp == ", ebp)

        console.log(hexdump(ebp, {
            offset: 0,
            length: 0x100,
            header: true,
            ansi: false
        }))

        console.log("arg0")
        console.log(args[0])

        console.log("arg1")
        console.log(args[1])

        console.log("arg2")
        console.log(args[2])

        console.log("arg3")
        console.log(args[3])
	},

	onLeave(ret) {
        console.log("LEAVE WITH RET")
        console.log(ret)
    }
});