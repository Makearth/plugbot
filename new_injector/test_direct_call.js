var adobe_air_mod = Process.findModuleByName("Adobe AIR.dll")

console.log("Adobe air dll loaded at :", adobe_air_mod)

var dll_base = ptr(adobe_air_mod.base)


var target_func_addr = dll_base.add(0x463560)


var target_func = new NativeFunction(target_func_addr, 'int64', ['pointer', 'pointer', 'int', 'char'])

var plugbot = null;
recv("swf", function onMessage(swf_message) {
	console.log("received swf message, replacing swf payload");
	console.log(swf_message)
	// console.log(swf_message.payload)
	plugbot = swf_message.payload
	send("Received swf data, all good !");
})

console.log("Attaching...")
var replaced = false
var it = Interceptor.attach(target_func_addr, {
	onEnter(args) {

    var buf = args[1];
    var size = args[2].toInt32();

    // works with size == 0x40f
    if (!replaced && plugbot != null && size == 0x40f)
    {
        var magic = buf.readCString(3)
        console.log("magic is : ", magic, " with size 0x" + size.toString(16))
        if (magic == "CWS" || magic == "FWS" || magic == "ZWS")
        // if (magic == "FWS" || magic == "ZWS")
        // if (magic == "CWS" && size == 0x10000)
        {

            // \x0aSoundclass\x00
            var scans = Memory.scanSync(buf, 0x80, "0a 53 6f 75 6e 64 43 6c 61 73 73 00")
            if (scans.length > 0)
            {
                console.log("SoundClass, skipping...")
                return
            }

            console.log("This: ", args[0]);
            console.log("Content: ", buf);
            console.log("Size: 0x" + size.toString(16));
            console.log("flags: ", args[3]);

            console.log(hexdump(buf, {
                offset: 0,
                length: 0x100,
                header: true,
                ansi: false
            }))


            var f = new File("used_for_inject.swf", "wb")
            f.write(buf.readByteArray(size))
            f.close()

            console.log("Writing plugbot in memory with size ", plugbot.length)
            var dst_buf = Memory.alloc(plugbot.length)
            dst_buf.writeByteArray(plugbot)

            target_func(args[0], dst_buf, plugbot.length, 1)
            console.log("Injected plugbot !")

            it.detach()
            replaced = true
        }
    }

	},

	onLeave(ret) {
    }
});